//validate battletag format

function validateForm(){
  let battleTag = document.getElementById("inputBattletag");

  let rgex = /^[a-zA-z]{3,20}[-][0-9]{5}$/ ;

  let result = rgex.test(battleTag.value);

  if(!result){
    alert("Battletag must begin with letters and end with 5 numbers.")
  }

  if(result){
  
    loadDoc(battleTag.value);
  }
  
}

//load required information
function loadDoc(value) {

  var xhttp = new XMLHttpRequest();

  xhttp.onreadystatechange = function() {

    if (this.readyState == 4 && this.status == 200) {
      

      var myObj = JSON.parse(this.responseText);

    document.getElementById("classement").innerHTML = "Classement:" + myObj.rating;

    document.getElementById("victoire").innerHTML = "Vistoires:" + myObj.quickPlayStats.games.won;

    document.getElementById("partie").innerHTML = "Parties joué:" + myObj.quickPlayStats.games.played;

    var gameLost = myObj.quickPlayStats.games.played - myObj.quickPlayStats.games.won;

    document.getElementById("defait").innerHTML = "Défaites:" + gameLost;

    //console.log(myObj);

    
    } else if (this.readyState == 4 && this.status == 404) {
      alert ("battletag inexistant")
    }
  };
  xhttp.open("GET", "https://ow-api.com/v1/stats/pc/us/" + value + "/profile", true);
  xhttp.send();

}

inputBirthday.max = new Date().toISOString().split("T")[0];

//remplacer le # avec -

document.getElementById("inputBattletag").onkeyup = function() {validateBattleTag()};

function validateBattleTag() {
let battleTag = document.getElementById("inputBattletag");
battleTag.value = battleTag.value.replace("#","-");

}


document.getElementById("submit").addEventListener("click", function(event){
  event.preventDefault()
});

document.getElementById("submit").addEventListener("click", validateForm);

